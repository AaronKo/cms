/**
 * Created by Aaron on 4/27/2016.
 */
var express = require('express');
var router = express.Router();

//var Page = require('./admin.js').Page;
var mongoose = require('mongoose');
var Page = mongoose.model('Page');

router.get('/:url', function(req, res, next) {
    Page.findOne({url: req.params.url}, function(err, doc){
        console.log(req.params.url + "end");
        if (doc==null){
            res.redirect("/");
        }
        else{
            console.log(doc);
            res.render("pages/dePage", doc);
        }
    });
});

module.exports = router;