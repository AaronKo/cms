/**
 * Created by Aaron on 4/27/2016.
 */
var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/cms');

var Page = mongoose.model('Page',{
    title: String,
    body: String,
    url: String
});

/* GET home page. */
router.get('/', function(req, res, next) {
    Page.find({}, function(err, Pages){
        res.render('admin/admin', {
            title: 'Pages',
            pages: Pages
        });
    });
});

router.get('/newPage', function(req, res){
    res.render('admin/newPage', {title: 'New Page'});
});

router.post('/page/:id', function(req, res){
    if (req.body.action == "delete"){
        Page.remove({_id: req.params.id}, function(err, doc){
            res.redirect("/admin/");
        });
    }

    if (req.body.action == "edit"){
        var update = {
            title: req.body.title,
            body: req.body.body,
            url: req.body.url
        };
        Page.update({_id: req.params.id}, update, function(err, doc){
            res.redirect("/admin/");
        });
    };
});

router.get('/page/:id', function(req, res){
    Page.findOne({_id: req.params.id}, function(err, doc){
        res.render('admin/editPage', doc);
    });
});

router.post('/newPage', function(req, res){
    var newPage = Page(req.body);
    newPage.save(function(err, doc){
        res.redirect("/admin/")
    });
});

router.post('/contentEdit', function(req, res){
    console.log(req.body);
    Page.update({_id: req.body._id}, req.body, function(err, doc){
        if(err){throw err;}
        res.status(200).json({data: doc});
    });
});

module.exports = router;