/**
 * Created by Aaron on 4/29/2016.
 */
window.addEventListener('load', function() {
    var editor;
    editor = ContentTools.EditorApp.get();
    editor.init('*[data-editable]', 'data-name');

    editor.addEventListener('saved', function (ev) {
        var name, payload, regions, xhr;

        // Check that something changed
        regions = ev.detail().regions;
        if (Object.keys(regions).length == 0) {
            return;
        }

        console.log(regions);
        var hidid = document.getElementById('hidden_id').innerText;
        var title = $("#title").text();
        var body = $("#body").html();

        // Set the editor as busy while we save our changes
        this.busy(true);

        // Collect the contents of each region into a FormData instance
        var payload = {
            _id: hidid,
            title: title,
            body: body
        };


        $.ajax({
            dataType: 'json',
            url: "/admin/contentEdit",
            type: "post",
            data: payload,
            success: function()
            {
                console.log("data saved");
                editor.busy(false);
                new ContentTools.FlashUI('ok');
            }
        });
    });
});
